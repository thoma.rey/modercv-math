# ModernCV académique

Initialement simplement un template pour faire de jolis cv académiques avec une classe LaTeX moderncv légèrement modifiée, a progressivement évoluée en un cours pour la communication de résultats (académique, scientifiques) avec LaTeX. Requiert les paquets (contenus dans Texlive2019)
- moderncv
- moderntimeline
- graphicx
- beamer
- bibtex

Des commentaires dans le code illustrent les différentes possibilités prévues par la classe utilisée. 

Un cours sur Beamer et un cours sur BibTeX figurent dans ce repo, pour faire toute la chaine de présentation académique.

![Alt-Text](<./example.png>)
